from settings import red, black, white, title
from button import Button
import pygame


def main(screen):
    width = 800
    pygame.display.set_caption(f"{title} | Menu")

    buttons = pygame.sprite.Group()
    buttons.add(
        Button(300, 300, 200, 50, "Games", 1),
        Button(300, 360, 200, 50, "Levels", 2),
        Button(300, 420, 200, 50, "Final", 3),
        Button(300, 480, 200, 50, "Exit", -1),
    )

    selected_button = buttons.sprites()[0]
    selected_button.selected = True

    next_screen = 0
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                next_screen = -1
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                for num, button in enumerate(buttons):
                    if button.is_clicked(mouse_pos):
                        selected_button.selected = False
                        selected_button = button
                        selected_button.selected = True
                        running = False
                        next_screen = num + 1
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    next_screen = -1                    
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    selected_button.selected = False
                    if event.key == pygame.K_UP:
                        selected_button = buttons.sprites()[(buttons.sprites().index(selected_button) - 1) % len(buttons.sprites())]
                    elif event.key == pygame.K_DOWN:
                        selected_button = buttons.sprites()[(buttons.sprites().index(selected_button) - 1) % len(buttons.sprites())]
                    selected_button.selected = True

        # Очистка экрана
        screen.fill(black)

        # Рисование надписи
        font = pygame.font.Font(None, 72)
        text = font.render(title, True, white)
        text_rect = text.get_rect(center=(width // 2, 100))
        screen.blit(text, text_rect)

        # Рисование кнопок
        for button in buttons:
            button.draw(screen)

        # Обновление экрана
        pygame.display.flip()
    return {'screen': next_screen, 'level': None}


if __name__ == "__main__":
    width = 800
    height = 600
    pygame.init()
    screen = pygame.display.set_mode(size=(width, height))
    print(main(screen))
    pygame.quit()
