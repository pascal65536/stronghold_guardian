import file_json


def get_board(word='', width=30, height=12):
    zx_font = file_json.load_json("data", "zx_font.json")

    array = [[] for _ in range(8)]
    for i, letter in enumerate(word):
        for j in range(8):
            array[j].extend(zx_font[letter][j])

    board = list()
    for row in array:
        if len(row) > width:
            board.append(row[:width])
        else:
            board.append(row + [0 for _ in range(width - len(row))])
    return board


if __name__ == "__main__":
    print('.' * 50)
    board_lst = get_board(word='board')
    for board in board_lst:
        print(board)
