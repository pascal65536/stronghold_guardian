import file_json as fj
import pygame
import menu
import level
import game
import final


lvl = None


def menus(screen):
    """
    Menu
    """
    return menu.main(screen)["screen"]


def games(screen):
    """
    Games
    """
    global lvl

    if lvl is None:
        score_dct = fj.load_json("data", "score.json")
        ex_keys = ["points", "maximum", "total"]
        lvl_lst = [(score_dct[z], z) for z in score_dct if z not in ex_keys]
        level_lst = list(filter(lambda x: x[0] < 1, lvl_lst))
        level_lst.sort(reverse=True)
        lvl = level_lst[0][1] if level_lst else 3
    games_dct = game.main(screen, int(lvl))
    lvl = games_dct["level"]
    return games_dct["screen"]


def levels(screen):
    """
    Levels
    """
    global lvl

    score_dct = fj.load_json("data", "score.json")
    ex_keys = ["points", "maximum", "total"]
    user_level_dct = dict((z, score_dct[z]) for z in score_dct if z not in ex_keys)
    levels_dct = level.main(screen, user_level_dct)
    lvl = levels_dct["level"]
    return levels_dct["screen"]


def finals(screen):
    """
    Final
    """
    score_dct = fj.load_json("data", "score.json")
    lvl_keys = ["maximum", "total"]
    level_lst = list((z, score_dct[z]) for z in score_dct if z in lvl_keys)
    level_lst.sort(reverse=False)
    score_lst = list((z, score_dct[z]) for z in score_dct if len(z) == 1)
    score_lst.sort()
    levels_dct = final.main(screen, [level_lst, score_lst])
    return levels_dct["screen"]


def switch_scene(scene):
    global current_scene
    current_scene = scene


scene_lst = [menus, games, levels, finals, None]

if __name__ == "__main__":
    width = 800
    height = 600
    pygame.init()
    screen = pygame.display.set_mode(size=(width, height))
    switch_scene(scene_lst[0])
    while current_scene is not None:
        switch_scene(scene_lst[current_scene(screen)])
    pygame.quit()
