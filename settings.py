import getpass
import os

red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
brown = (165, 42, 42)
gray = (200, 200, 200)
black = (0, 0, 0)
white = (255, 255, 255)
darkgray = (20, 20, 20)

title = "Bugаnoid"

level_lst = {0: "", 1: os.name, 2: getpass.getuser(), 3: "hold", 4: "board"}
