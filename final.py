from settings import gray, white, title, darkgray
from button import Button
import file_json as fj
import pygame


def main(screen, field_lst=[[], []]):
    level_lst = field_lst[0]
    score_lst = field_lst[1]
   
    width = 800
    pygame.display.set_caption(f"{title} | Final")

    buttons = pygame.sprite.Group()
    buttons = pygame.sprite.Group()
    buttons.add(
        Button(300, 420, 200, 50, "Start", 0),
        Button(300, 480, 200, 50, "Exit", -1),
    )

    selected_button = buttons.sprites()[0]
    selected_button.selected = True

    level = None
    next_screen = 0
    running = True
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                next_screen = 0
            elif event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pos = pygame.mouse.get_pos()
                for button in buttons:
                    if button.is_clicked(mouse_pos):
                        selected_button.selected = False
                        selected_button = button
                        selected_button.selected = True
                        running = False
                        next_screen = selected_button.target
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    next_screen = 0
                elif event.key == pygame.K_UP or event.key == pygame.K_DOWN:
                    selected_button.selected = False
                    if event.key == pygame.K_UP:
                        selected_button = buttons.sprites()[
                            (buttons.sprites().index(selected_button) - 1)
                            % len(buttons.sprites())
                        ]
                    elif event.key == pygame.K_DOWN:
                        selected_button = buttons.sprites()[
                            (buttons.sprites().index(selected_button) - 1)
                            % len(buttons.sprites())
                        ]
                    selected_button.selected = True

        # Очистка экрана
        screen.fill(darkgray)

        # Рисование надписи
        font = pygame.font.Font(None, 72)
        text = font.render(f"Final", True, white)
        text_rect = text.get_rect(center=(width // 2, 100))
        screen.blit(text, text_rect)

        top = 160
        border = 250
        font = pygame.font.SysFont(None, 50)
        for num, final_tpl in enumerate(level_lst):
            text = font.render(final_tpl[0], True, gray)
            screen.blit(text, (border, top + num * 60))
            text = font.render(f'{final_tpl[1]:.2f}', True, gray)
            screen.blit(text, (width - border - text.get_rect()[2], top + num * 60))

        top = 300
        border = 150
        border_center = 50
        font = pygame.font.SysFont(None, 40)
        for num in range(0, len(score_lst) - 1, 2):
            lf = score_lst[num]           
            text = font.render(f'Level {lf[0]}', True, gray)
            screen.blit(text, (border, top + num * 20))
            text = font.render(f'{lf[1]:.2f}', True, gray)
            screen.blit(text, ((width // 2) - border_center - text.get_rect()[2], top + num * 20))

            rt = score_lst[num + 1]
            text = font.render(f'Level {rt[0]}', True, gray)
            screen.blit(text, ((width // 2) + border_center, top + num * 20))
            text = font.render(f'{rt[1]:.2f}', True, gray)
            screen.blit(text, (width - border - text.get_rect()[2], top + num * 20))


        # Рисование кнопок
        for button in buttons:
            button.draw(screen)

        # Обновление экрана
        pygame.display.flip()
    return {"screen": next_screen, "level": level}


if __name__ == "__main__":
    width = 800
    height = 600
    pygame.init()
    screen = pygame.display.set_mode(size=(width, height))
    print(main(screen))
    pygame.quit()
