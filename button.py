from settings import red, black, white, title
import pygame


class Button(pygame.sprite.Sprite):
    def __init__(self, x, y, width, height, text, target):
        super().__init__()

        self.image = pygame.Surface((width, height))
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y

        self.target = target
        self.text = text
        self.selected = False

    def draw(self, screen):
        font = pygame.font.Font(None, 36)
        text = font.render(self.text, True, black)
        text_rect = text.get_rect(center=self.rect.center)
        if self.selected:
            pygame.draw.rect(screen, white, self.rect)
            pygame.draw.rect(screen, red, self.rect, 3)
        else:
            pygame.draw.rect(screen, white, self.rect)
        screen.blit(text, text_rect)

    def is_clicked(self, mouse_pos):
        return self.rect.collidepoint(mouse_pos)
