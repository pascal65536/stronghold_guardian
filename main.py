from settings import red, green, blue, brown, gray, black, white
import file_json as fj
import random
import pygame
import math
import os


class Rocket:
    def __init__(self, screen):
        self.width = 150
        self.height = 30
        self.x = screen.get_width() // 2 - self.width // 2
        self.y = screen.get_height() - self.height - 10
        self.rect = pygame.Rect([self.x, self.y, self.width, self.height])
        self.speed = 5
        self.is_gluey = True

    def movie(self, screen):
        if self.x < 0:
            self.x = 0
        if self.x > screen.get_width() - self.width:
            self.x = screen.get_width() - self.width
        self.rect = pygame.Rect([self.x, self.y, self.width, self.height])

    def __repr__(self):
        return f"{self.x} {self.y}"


class Ball:
    def __init__(self, sprite_name, obj_name, collide_count=1):
        self.name = obj_name
        file_name = os.path.join("data", sprite_name)
        self.image = pygame.image.load(file_name)
        self.rect = self.image.get_rect()
        self.x = None
        self.y = None
        self.speed_x = None
        self.speed_y = None
        self.dr = 10
        self.dc = 10
        self.collide_count = collide_count
        self.score = 1

    def apply(self, rocket):
        self.x = rocket.x + rocket.width / 2 - self.image.get_height() / 2
        self.y = rocket.y - self.image.get_height()
        self.speed_x = 0
        self.speed_y = 0
        self.collide_count = 5

    def movie(self, screen):
        self.x += self.speed_x
        self.y -= self.speed_y
        self.rect.x = self.x
        self.rect.y = self.y
        if self.x <= 0 or self.x >= screen.get_width() - self.rect.width:
            self.speed_x *= -1
        if self.y <= 0 or self.y >= screen.get_height() - self.rect.height:
            self.speed_y *= -1

    def set_ball(self, x, y, dx, dy):
        self.x = x
        self.y = y
        self.rect.x = self.x
        self.rect.y = self.y
        self.speed_x = dx
        self.speed_y = dy

    def collide(self, sprite_lst, rocket):
        score = 0
        if self.rect.colliderect(rocket.rect):
            self.speed_y = -1 * self.speed_y
            # self.speed_x = -1 * self.speed_x
            return score

        for obj in sprite_lst:
            if self.rect.colliderect(obj.rect):
                obj.collide_count -= 1
                if obj.collide_count < 1:
                    obj_index = sprite_lst.index(obj)
                    pointer = sprite_lst.pop(obj_index)
                    score = pointer.score
                    
                self.collide_count -= 1
                if self.collide_count < 1:
                    self.collide_count = 5
                    self.speed_x += random.random() - 0.5
                    self.speed_y += random.random() - 0.5
                
                if self.speed_x > 0:
                    self.speed_x = -abs(self.speed_x)
                else:
                    self.speed_x = abs(self.speed_x)
                if self.speed_y > 0:
                    self.speed_y = -abs(self.speed_y)
                else:
                    self.speed_y = abs(self.speed_y)
                if obj.speed_x > 0:
                    obj.speed_x = -abs(obj.speed_x)
                else:
                    obj.speed_x = abs(obj.speed_x)
                if obj.speed_y > 0:
                    obj.speed_y = -abs(obj.speed_y)
                else:
                    obj.speed_y = abs(obj.speed_y)

                return score
        return score

    def __repr__(self):
        return f"{self.name} {self.x} {self.y}"


class Game:
    def __init__(self, width, height, footer, caption):
        score_dct = fj.load_json("data", "score.json")

        self.width = width
        self.height = height
        self.footer = footer
        self.total = score_dct.get("total", 0)
        self.maximum = score_dct.get("maximum", 0)

        self.window = pygame.display.set_mode((width, height))
        pygame.display.set_caption(caption)
        self.window = pygame.display.set_mode((width, height))
        self.window.fill(white)

        self.screen_height = height - footer
        self.screen = pygame.Surface((self.width, self.screen_height))
        self.screen.fill(red, rect=(0, 0, self.width, self.screen_height))

        self.control_surface = pygame.Surface((self.width, footer))
        self.control_surface.fill(blue, rect=(0, 0, self.width, footer))

        self.points = 0
        self.level()

    def level(self):
        sprite_lst = []
        for row in range(30):
            for col in range(12):
                sprite = Ball("sprite0.png", "static")
                sprite.set_ball(sprite.dr + (26 * row), sprite.dc + (26 * col), 0, 0)
                sprite_lst.append(sprite)

        rocket = Rocket(self.screen)
        gun = Ball("gun.png", "gun")
        gun.apply(rocket)

        running = True
        while running:
            for event in pygame.event.get():
                if event.type == pygame.QUIT:
                    running = False
                elif event.type == pygame.KEYDOWN:
                    if event.key == pygame.K_ESCAPE:
                        running = False
                    if event.key == pygame.K_SPACE:
                        if not rocket.is_gluey:
                            pass
                        gun.collide_count = 10
                        rocket.is_gluey = False
                        angle = 1 + .2 - random.random() / 10
                        amplifier = gun.image.get_height() // 3
                        gun.speed_y = amplifier * math.sin(angle)
                        gun.speed_x = amplifier * math.cos(angle)

            keys = pygame.key.get_pressed()
            if keys[pygame.K_LEFT]:
                rocket.x -= rocket.speed
            if keys[pygame.K_RIGHT]:
                rocket.x += rocket.speed

            self.screen.fill(gray)
            for sprite in sprite_lst:
                self.screen.blit(sprite.image, (sprite.x, sprite.y))

            rocket.movie(self.screen)
            pygame.draw.rect(self.screen, brown, (rocket.rect))
            if rocket.is_gluey:
                gun.apply(rocket)
            gun.movie(self.screen)
            self.screen.blit(gun.image, (gun.x, gun.y))
            self.points += gun.collide(sprite_lst, rocket)
            if gun.y > rocket.y:
                running = False
            pygame.display.flip()
            self.window.blit(self.screen, (0, 0))
            if len(sprite_lst) < 1:
                running = False
            self.show_score()
            self.maximum = max([self.maximum, self.points])
            score_dct = {
                "points": self.points,
                "maximum": self.maximum,
                "total": self.total + self.points,
            }
        fj.save_json("data", "score.json", score_dct)

    def show_score(self):
        bottom_rect = pygame.Rect(0, 0, self.width, self.footer)
        pygame.draw.rect(self.control_surface, white, bottom_rect)

        font = pygame.font.Font(None, 30)
        text = font.render(f"Points: {self.points}", True, black)
        self.control_surface.blit(text, (10, 4))

        font = pygame.font.Font(None, 30)
        text = font.render(f"Total: {self.total}", True, black)
        self.control_surface.blit(text, (180, 4))

        font = pygame.font.Font(None, 30)
        text = font.render(f"Maximum: {self.maximum}", True, black)
        self.control_surface.blit(text, (300, 4))

        pygame.display.flip()
        self.window.blit(self.control_surface, (0, self.screen_height))


if __name__ == "__main__":

    pygame.init()
    Game(800, 600, 30, "Stronghold Guardian")
    pygame.quit()
