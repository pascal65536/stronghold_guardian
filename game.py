from settings import red, blue, brown, gray, black, white, title, level_lst
import file_json as fj
import random
import pygame
import math
import os
import zx_font


class Rocket:
    def __init__(self, screen):
        self.width = 150
        self.height = 30
        self.x = screen.get_width() // 2 - self.width // 2
        self.y = screen.get_height() - self.height - 10
        self.rect = pygame.Rect([self.x, self.y, self.width, self.height])
        self.speed = 3
        self.is_gluey = True

    def movie(self, screen):
        if self.x < 0:
            self.x = 0
        if self.x > screen.get_width() - self.width:
            self.x = screen.get_width() - self.width
        self.rect = pygame.Rect([self.x, self.y, self.width, self.height])


class Ball:
    def __init__(self, sprite_name, collide_count=1):
        file_name = os.path.join("data", sprite_name)
        self.image = pygame.image.load(file_name)
        self.rect = self.image.get_rect()
        self.x = None
        self.y = None
        self.speed_x = None
        self.speed_y = None
        self.dr = 10
        self.dc = 10
        self.collide_count = collide_count
        self.score = 1

    def apply(self, rocket):
        self.x = rocket.x + rocket.width / 2 - self.image.get_height() / 2
        self.y = rocket.y - self.image.get_height()
        self.speed_x = 0
        self.speed_y = 0
        self.collide_count = 5

    def movie(self, screen):
        self.x += self.speed_x
        self.y -= self.speed_y
        self.rect.x = self.x
        self.rect.y = self.y
        if self.x <= 0 or self.x >= screen.get_width() - self.rect.width:
            self.speed_x *= -1
        if self.y <= 0 or self.y >= screen.get_height() - self.rect.height:
            self.speed_y *= -1

    def set_ball(self, x, y, dx, dy):
        self.x = x
        self.y = y
        self.rect.x = self.x
        self.rect.y = self.y
        self.speed_x = dx
        self.speed_y = dy

    def collide(self, sprite_lst, rocket, kpi=0):
        score = 0
        if self.rect.colliderect(rocket.rect):
            self.speed_y = -1 * self.speed_y
            # self.speed_x = -1 * self.speed_x
            return score, kpi

        for obj in sprite_lst:
            if self.rect.colliderect(obj.rect):
                obj.collide_count -= 1
                score = obj.score

                if obj.collide_count < 1:
                    obj_index = sprite_lst.index(obj)
                    pointer = sprite_lst.pop(obj_index)
                    kpi += 1

                self.collide_count -= 1
                if self.collide_count < 1:
                    self.collide_count = 5
                    self.speed_x += random.random() - 0.5
                    self.speed_y += random.random() - 0.5

                if self.speed_x > 0:
                    self.speed_x = -abs(self.speed_x)
                else:
                    self.speed_x = abs(self.speed_x)
                if self.speed_y > 0:
                    self.speed_y = -abs(self.speed_y)
                else:
                    self.speed_y = abs(self.speed_y)
                if obj.speed_x > 0:
                    obj.speed_x = -abs(obj.speed_x)
                else:
                    obj.speed_x = abs(obj.speed_x)
                if obj.speed_y > 0:
                    obj.speed_y = -abs(obj.speed_y)
                else:
                    obj.speed_y = abs(obj.speed_y)

                return score, kpi
        return score, kpi


def main(window, level=0):
    width = 800
    height = 600
    footer = 30

    def get_level(level):

        board_lst = zx_font.get_board(level_lst.get(level, str(level)))
        sprite_lst = []
        for row_num, row in enumerate(board_lst):
            for col_num, col in enumerate(row):
                sprite = Ball(f"sprite{col}.png", collide_count=col + 1)
                x = sprite.dr + (26 * col_num)
                y = sprite.dc + (26 * row_num)
                sprite.set_ball(x, y, 0, 0)
                sprite_lst.append(sprite)

        return sprite_lst

    def print_score(msg, left):
        font = pygame.font.Font(None, 30)
        text = font.render(msg, True, black)
        control_surface.blit(text, (left, 4))

    def show_score():
        bottom_rect = pygame.Rect(0, 0, width, footer)
        pygame.draw.rect(control_surface, white, bottom_rect)

        print_score(f"Points: {points}", 15)
        print_score(f"Total: {total}", 140)
        print_score(f"Maximum: {maximum}", 280)
        print_score(f"KPI*: {prew_kpi:.2f}", 450)
        print_score(f"KPI: {kpi / divide_kpi:.2f}", 550)
        print_score(f"Level: {level}", 650)
        
        pygame.display.flip()
        window.blit(control_surface, (0, screen_height))

    score_dct = fj.load_json("data", "score.json")
    total = score_dct.get("total", 0)
    maximum = score_dct.get("maximum", 0)
    prew_kpi = score_dct.get(str(level), 0)

    pygame.display.set_caption(title)
    window.fill(white)

    screen_height = height - footer
    screen = pygame.Surface((width, screen_height))
    screen.fill(red, rect=(0, 0, width, screen_height))

    control_surface = pygame.Surface((width, footer))
    control_surface.fill(blue, rect=(0, 0, width, footer))

    sprite_lst = get_level(level)
    divide_kpi = len(sprite_lst)

    rocket = Rocket(screen)
    gun = Ball("gun.png")
    gun.apply(rocket)

    kpi = 0
    points = 0
    next_screen = 0
    running = True
    next_level = level
    while running:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
                next_screen = 0
                next_level = level
            elif event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    running = False
                    next_screen = 0
                    next_level = level
                if event.key == pygame.K_SPACE:
                    if rocket.is_gluey:
                        gun.collide_count = 10
                        rocket.is_gluey = False
                        angle = 1 + 0.2 - random.random() / 5
                        amplifier = gun.image.get_height() // 3
                        gun.speed_y = amplifier * math.sin(angle)
                        gun.speed_x = amplifier * math.cos(angle)

        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            rocket.x -= rocket.speed
        if keys[pygame.K_RIGHT]:
            rocket.x += rocket.speed

        screen.fill(gray)
        for sprite in sprite_lst:
            screen.blit(sprite.image, (sprite.x, sprite.y))

        rocket.movie(screen)
        pygame.draw.rect(screen, brown, (rocket.rect))
        if rocket.is_gluey:
            gun.apply(rocket)
        gun.movie(screen)
        screen.blit(gun.image, (gun.x, gun.y))

        if gun.y > rocket.y:
            running = False
            next_screen = 2
            next_level = None
        pygame.display.flip()
        window.blit(screen, (0, 0))
        if len(sprite_lst) < 1:
            running = False
            next_screen = 2
            next_level = None

        pnts, kpi = gun.collide(sprite_lst, rocket, kpi)
        points += pnts
        maximum = max([maximum, points])
        score_dct.update(
            {
                "points": points,
                "maximum": maximum,
                "total": total + points,
                str(level): max(kpi / divide_kpi, prew_kpi),
            }
        )
        show_score()

    fj.save_json("data", "score.json", score_dct)
    return {'screen': next_screen, 'level': next_level}


if __name__ == "__main__":
    width = 800
    height = 600
    pygame.init()
    screen = pygame.display.set_mode(size=(width, height))
    print(main(screen))
    pygame.quit()
